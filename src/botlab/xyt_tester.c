#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
/*
#include "math/gsl_util_vector.h"
#include "math/gsl_util_matrix.h"
#include "math/gsl_util_blas.h"
*/
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
//#include <gsl/gsl_blas.h>
#include "xyt.h"

int main() {

gsl_vector  * X_ij = gsl_vector_calloc(3);
gsl_vector * X_jk = gsl_vector_calloc(3);
gsl_vector * X_ik = gsl_vector_calloc(3);
gsl_vector * X_jk_test = gsl_vector_calloc(3);

gsl_vector_set(X_ij,0,1);
gsl_vector_set(X_jk,1,1);
gsl_vector_set(X_jk, 2, M_PI/4.0);


xyt_head2tail_gsl(X_ik,NULL,X_ij,X_jk);

gsl_vector_fprintf(stdout, X_ik, "%f");

xyt_tail2tail_gsl(X_jk_test, NULL, X_ij, X_ik) ;

gsl_vector_fprintf(stdout, X_jk_test, "%f");

gsl_vector_free(X_ij);
gsl_vector_free(X_jk);
gsl_vector_free(X_ik);
}

