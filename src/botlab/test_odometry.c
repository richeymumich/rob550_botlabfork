#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>


#include "common/getopt.h"
#include "math/gsl_util_matrix.h"
#include "math/gsl_util_blas.h"
#include "math/math_util.h"

#include "xyt.h"


void print_matrix(gsl_matrix * MatIn, int rows, int cols, double scale)
{
	printf("\n\n");
	for(int i = 0; i < rows; i++){
		for(int j = 0; j < cols; j++){
        		printf("%f\t",gsl_matrix_get(MatIn,i,j)*scale);
		}
		printf("\n");
	}
	printf("\n\n");
}

int main(void)
{

    state_t *state = user;
    double dL, dR, sigmaL, sigmaR, sigmaS, dS;
 
	int state_encoder_prev_0 = 0; // state->encoder_prev[0]
	int state_encoder_prev_1 = 0; // state->encoder_prev[1]

	int msg_left_ticks = 100; // msg->encoder_left_ticks
	int msg_right_ticks = 100; // msg->encoder_right_ticks

	double state_b = .08; //state->b

	double state_beta = .05; //state_beta
	double state_alpha = .05; //state->alpha

	double * state_sigma = calloc(9,sizeof(double)); //state->Sigma

    gsl_matrix * Jplus = gsl_matrix_calloc(3, 6);
    gsl_vector * Delta = gsl_vector_calloc(3);
    gsl_vector * P = gsl_vector_calloc(3);
    gsl_vector * Pprime = gsl_vector_calloc(3);
    gsl_matrix * SigmaPprime = gsl_matrix_calloc(3, 3);
    gsl_matrix * SigmaD = gsl_matrix_calloc(3, 3);
    gsl_matrix * SigmaPD = gsl_matrix_calloc(6, 6);
    gsl_matrix * littleSigma = gsl_matrix_calloc(3, 3);
    gsl_matrix * A = gsl_matrix_calloc(3, 3);
    gsl_matrix * temp = gsl_matrix_calloc(6, 3);
    gsl_matrix *temp2 = gsl_matrix_calloc(3,3);

    //Populate A
    gsl_matrix_set(A, 0, 0, 0.5);
    gsl_matrix_set(A, 0, 1, 0.5);
    gsl_matrix_set(A, 1, 2, 1.0);
    gsl_matrix_set(A, 2, 0, -1.0/state_b);
    gsl_matrix_set(A, 2, 1, 1.0/state_b);

   
    //Find dL, dR, dS, Delta
    if(state_encoder_prev_0 == -1.0){
	state_encoder_prev_0 = msg_left_ticks;
	state_encoder_prev_1 = msg_right_ticks;
    }
    dL = (msg_left_ticks - state_encoder_prev_0)*state->meters_per_tick;
    dR = (msg_right_ticks - state_encoder_prev_1)*state->meters_per_tick;

    dS = 0.0;
    gsl_vector_set(Delta, 0, (dR+dL)/2.0);
    gsl_vector_set(Delta, 1, dS);
    gsl_vector_set(Delta, 2, (dR-dL)/state_b);

    //Populate littleSigma
    sigmaL = state_alpha * dL;
    sigmaR = state_alpha * dR;
    sigmaS = state_beta * (dR+dL);

    gsl_matrix_set(littleSigma, 0, 0, sigmaL*sigmaL);
    gsl_matrix_set(littleSigma, 1, 1, sigmaR*sigmaR);
    gsl_matrix_set(littleSigma, 2, 2, sigmaS*sigmaS);

    //Populate SigmaD
    gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, littleSigma, A, 0.0, temp2);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, temp2, 0.0, SigmaD);



    //Populate SigmaPD
    for(int i=0; i < 6; i++){
	for(int j=0; j < 6; j++){
		if(i < 3 && j < 3){
			gsl_matrix_set(SigmaPD, i, j, state_sigma[i*6+j]);
		}
		else if(i >= 3 && j >= 3){
			gsl_matrix_set(SigmaPD, i, j, gsl_matrix_get(SigmaD, i-3, j-3));
		}
	}
    }

    //populate p vector
    gsl_vector_set(P, 0, state->xyt[0]);
    gsl_vector_set(P, 1, state->xyt[1]);
    gsl_vector_set(P, 2, state->xyt[2]);

    xyt_head2tail_gsl(Pprime, Jplus, P, Delta);

    //Find SigmaPprime
    gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, SigmaPD, Jplus, 0.0, temp);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Jplus, temp, 0.0, SigmaPprime);

printf("Little Sigma\n");
print_matrix(littleSigma,2,2,1000000);
printf("sigmaD\n");
print_matrix(SigmaD,3,3,1000000);
printf("sigmaPD\n");
print_matrix(SigmaPD,6,6,1000000);
printf("Jplus\n");
print_matrix(Jplus,3,6,1);
printf("sigmaP'\n");
print_matrix(SigmaPprime,3,3,1000000);


    //Update State variables
 //   memcpy(state->xyt, Pprime->data, sizeof state->xyt);
   // memcpy(state->Sigma, SigmaPprime->data, sizeof state->Sigma); 
    
	// publish
   // pose_xyt_t odo = { .utime = msg->utime };
   // memcpy (odo.xyt, state->xyt, sizeof state->xyt);
   // memcpy (odo.Sigma, state->Sigma, sizeof state->Sigma);
//    pose_xyt_t_publish (state->lcm, state->odometry_channel, &odo);
 
    //Update encoder_prev values to be current values
  //  state->encoder_prev[0] = msg->encoder_left_ticks;
   // state->encoder_prev[1] = msg->encoder_right_ticks;

    gsl_matrix_free(Jplus);
    gsl_vector_free(Delta);
    gsl_vector_free(P);
    gsl_vector_free(Pprime);
    gsl_matrix_free(SigmaPprime);
    gsl_matrix_free(SigmaD);
    gsl_matrix_free(SigmaPD);
    gsl_matrix_free(littleSigma);
    gsl_matrix_free(A);
    gsl_matrix_free(temp);

}
