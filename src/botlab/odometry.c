//-------------------------------------------------
//----------ODOMETRY.C-----------------------------
//-------------------------------------------------
//AUTHORS: Wyatt Felt, Katelyn Fry, Meghan Richey
//DUE: 12/14/14
//-------------------------------------------------
//This program calculates the robot's estimated pose
//from the given LCM messages. It reports this pose
//to the GUI for rendering.
//-------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>

#include "common/getopt.h"
#include "math/gsl_util_matrix.h"
#include "math/gsl_util_blas.h"
#include "math/math_util.h"

#include "lcmtypes/maebot_motor_feedback_t.h"
#include "lcmtypes/maebot_sensor_data_t.h"
#include "lcmtypes/pose_xyt_t.h"

#include "xyt.h"

#define ALPHA_STRING          "1.0"  // longitudinal covariance scaling factor
#define BETA_STRING           "1.0"  // lateral side-slip covariance scaling factor
#define GYRO_RMS_STRING       "1.0"    // [deg/s]

typedef struct state state_t;
struct state {
    getopt_t *gopt;

    lcm_t *lcm;
    const char *odometry_channel;
    const char *feedback_channel;
    const char *sensor_channel;

    // odometry params
    double meters_per_tick; // conversion factor that translates encoder pulses into linear wheel displacement
    double alpha;
    double beta;
    double gyro_rms;

    bool use_gyro;
    int64_t dtheta_utime;
    double dtheta;
    double dtheta_sigma;
    double biasrate;
    int64_t gyro_int_prev[3];
    int64_t gyro_utime_prev;
    double gyronoise;

    double xyt[3]; // 3-dof pose
    double Sigma[3*3];

	//need to add previous and current encoder values
    double encoder_prev[2];
    double b;
};

void print_matrix(gsl_matrix * MatIn, int rows, int cols, double scale)
{
	printf("\n\n");
	for(int i = 0; i < rows; i++){
		for(int j = 0; j < cols; j++){
        		printf("%f\t",gsl_matrix_get(MatIn,i,j)*scale);
		}
		printf("\n");
	}
	printf("\n\n");
}

static void
motor_feedback_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                        const maebot_motor_feedback_t *msg, void *user)
{
    state_t *state = user;
    double dL, dR, sigmaL, sigmaR, sigmaS, dS;

    gsl_matrix * Jplus = gsl_matrix_calloc(3, 6);
    gsl_vector * Delta = gsl_vector_calloc(3);
    gsl_vector * P = gsl_vector_calloc(3);
    gsl_vector * Pprime = gsl_vector_calloc(3);
    gsl_matrix * SigmaPprime = gsl_matrix_calloc(3, 3);
    gsl_matrix * SigmaD = gsl_matrix_calloc(3, 3);
    gsl_matrix * SigmaPD = gsl_matrix_calloc(6, 6);
    gsl_matrix * temp = gsl_matrix_calloc(6, 3);
//    gsl_matrix * temp2 = gsl_matrix_calloc(3,3); 
	
    //Find dL, dR, dS
    if(state->encoder_prev[0] == -1.0){
	state->encoder_prev[0] = msg->encoder_left_ticks;
	state->encoder_prev[1] = msg->encoder_right_ticks;
    }
    dL = (msg->encoder_left_ticks - state->encoder_prev[0])*state->meters_per_tick;
    dR = (msg->encoder_right_ticks - state->encoder_prev[1])*state->meters_per_tick;

    dS = 0.0;


if (state->use_gyro)
{
// printf("using gyro"); fflush(stdout); 
    gsl_matrix * littleSigma = gsl_matrix_calloc(4, 4);
    gsl_matrix * A = gsl_matrix_calloc(3, 4);
    //gsl_matrix * temp = gsl_matrix_calloc(6, 3);
    gsl_matrix * temp2 = gsl_matrix_calloc(4,3);

    //Populate A
    gsl_matrix_set(A, 0, 0, 0.5);
    gsl_matrix_set(A, 0, 1, 0.5);
    gsl_matrix_set(A, 1, 2, 1.0);
    gsl_matrix_set(A, 2, 3, 1.0);

   
    //Find Delta
    gsl_vector_set(Delta, 0, (dR+dL)/2.0);
    gsl_vector_set(Delta, 1, dS);
    gsl_vector_set(Delta, 2, state->dtheta);

//	 printf("A and delta set"); fflush(stdout);

    //Populate littleSigma
    sigmaL = state->alpha * dL;
    sigmaR = state->alpha * dR;
    sigmaS = state->beta * (dR+dL);

    gsl_matrix_set(littleSigma, 0, 0, sigmaL*sigmaL);
    gsl_matrix_set(littleSigma, 1, 1, sigmaR*sigmaR);
    gsl_matrix_set(littleSigma, 2, 2, sigmaS*sigmaS);
    gsl_matrix_set(littleSigma, 3, 3, state->dtheta_sigma*state->dtheta_sigma);

  //       printf("about to multiply for sigmaD"); fflush(stdout);
    //Populate SigmaD
    gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, littleSigma, A, 0.0, temp2);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, temp2, 0.0, SigmaD);

//         printf("acaba de multiply for sigmaD"); fflush(stdout);

//printf("Little Sigma\n");
//print_matrix(littleSigma,2,2,1000000);

    gsl_matrix_free(littleSigma);
    gsl_matrix_free(A);
  //  gsl_matrix_free(temp);
    gsl_matrix_free(temp2);


}
else //if you're not using the gyro
{
    gsl_matrix * littleSigma = gsl_matrix_calloc(3, 3);
   gsl_matrix * A = gsl_matrix_calloc(3, 3);
    //gsl_matrix * temp = gsl_matrix_calloc(6, 3);
    gsl_matrix * temp2 = gsl_matrix_calloc(3,3);

    //Populate A
    gsl_matrix_set(A, 0, 0, 0.5);
    gsl_matrix_set(A, 0, 1, 0.5);
    gsl_matrix_set(A, 1, 2, 1.0);
    gsl_matrix_set(A, 2, 0, -1.0/state->b);
    gsl_matrix_set(A, 2, 1, 1.0/state->b);

   
    //Find Delta
    gsl_vector_set(Delta, 0, (dR+dL)/2.0);
    gsl_vector_set(Delta, 1, dS);
    gsl_vector_set(Delta, 2, (dR-dL)/state->b);

    //Populate littleSigma
    sigmaL = state->alpha * dL;
    sigmaR = state->alpha * dR;
    sigmaS = state->beta * (dR+dL);

    gsl_matrix_set(littleSigma, 0, 0, sigmaL*sigmaL);
    gsl_matrix_set(littleSigma, 1, 1, sigmaR*sigmaR);
    gsl_matrix_set(littleSigma, 2, 2, sigmaS*sigmaS);

	
    //Populate SigmaD
    gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, littleSigma, A, 0.0, temp2);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, temp2, 0.0, SigmaD);

//printf("Little Sigma\n");
//print_matrix(littleSigma,2,2,1000000);

    gsl_matrix_free(littleSigma);
    gsl_matrix_free(A);
  //  gsl_matrix_free(temp);
    gsl_matrix_free(temp2);
} //end of the else for not using the gyro



    //Populate SigmaPD
    for(int i=0; i < 6; i++){
	for(int j=0; j < 6; j++){
		if(i < 3 && j < 3){
			gsl_matrix_set(SigmaPD, i, j, state->Sigma[i*3+j]);
		}
		else if(i >= 3 && j >= 3){
			gsl_matrix_set(SigmaPD, i, j, gsl_matrix_get(SigmaD, i-3, j-3));
		}
	}
    }

    //populate p vector
    gsl_vector_set(P, 0, state->xyt[0]);
    gsl_vector_set(P, 1, state->xyt[1]);
    gsl_vector_set(P, 2, state->xyt[2]);

    xyt_head2tail_gsl(Pprime, Jplus, P, Delta);

    //Find SigmaPprime
    gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, SigmaPD, Jplus, 0.0, temp);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Jplus, temp, 0.0, SigmaPprime);




/*printf("sigmaD\n");
print_matrix(SigmaD,3,3,1000000);
printf("sigmaPD\n");
print_matrix(SigmaPD,6,6,1000000);
printf("Jplus\n");
print_matrix(Jplus,3,6,1);
printf("sigmaP'\n");
*/
print_matrix(SigmaPprime,3,3,1000000);
printf("State: %lf, %lf, %lf\n", state->xyt[0], state->xyt[1], state->xyt[2]);





    //Update State variables
    memcpy(state->xyt, Pprime->data, sizeof state->xyt);
    memcpy(state->Sigma, SigmaPprime->data, sizeof state->Sigma); 
    
	// publish
    pose_xyt_t odo = { .utime = msg->utime };
    memcpy (odo.xyt, state->xyt, sizeof state->xyt);
    memcpy (odo.Sigma, state->Sigma, sizeof state->Sigma);
    pose_xyt_t_publish (state->lcm, state->odometry_channel, &odo);
 
    //Update encoder_prev values to be current values
    state->encoder_prev[0] = msg->encoder_left_ticks;
    state->encoder_prev[1] = msg->encoder_right_ticks;






    gsl_matrix_free(Jplus);
    gsl_vector_free(Delta);
    gsl_vector_free(P);
    gsl_vector_free(Pprime);
    gsl_matrix_free(SigmaPprime);
    gsl_matrix_free(SigmaD);
    gsl_matrix_free(SigmaPD);
    gsl_matrix_free(temp);


}

//This is for the gyro!
static void
sensor_data_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                     const maebot_sensor_data_t *msg, void *user)
{
    state_t *state = user;

    if (!state->use_gyro)
        return;

if(state->gyro_utime_prev != 0)
{
 // averagebias is -106994.744 udeg/sec
 // stdev of the bias is 26010.72428 udeg/sec
   double elapsedtime = 0;
    state->dtheta_utime = msg->utime;    
    elapsedtime = ((state->dtheta_utime - state->gyro_utime_prev)/1000000.0); //in seconds

	   // integrated units are ticks-usecs
   // the datasheet indicates a relationship of  
   // I want degrees out and ticks - usecs / (131 ticks/(deg/sec))  
    //initial angle measure
    state->dtheta = (((msg->gyro_int[2] - state->gyro_int_prev[2])/131.0))/1000000.0; //in degrees 

        state->dtheta = state->dtheta*3.14159/180.0; //convert to radians

	//corrected for bias
    state->dtheta = state->dtheta + (state->biasrate*.05007);





//    printf("utime: %lld\t utimeprev: %lld \n",state->dtheta_utime,state->gyro_utime_prev);
//	printf("%f\t%f\n", state->biasrate*100000,elapsedtime*1000000);
//    printf("Biasrate: %f dtheta: %f elaspedtime: %f \n",state->biasrate*1000000,state->dtheta,elapsedtime);
}

    memcpy(state->gyro_int_prev, msg->gyro_int, 3*sizeof(int64_t));
    state->gyro_utime_prev = msg->utime;

}



int main (int argc, char *argv[]) {
    // so that redirected stdout won't be insanely buffered.
    setvbuf (stdout, (char *) NULL, _IONBF, 0);

    state_t *state = calloc (1, sizeof *state);

    state->meters_per_tick = 1.0/4774.6; // IMPLEMENT ME-DONE!
    state->biasrate = 0;
    state->encoder_prev[0] = -1.0;
    state->encoder_prev[1] = -1.0;
    state->gyro_int_prev[2] = 0;
    state->gyro_utime_prev = 0;
    state->biasrate =  1/54.2; //rads/sec     // -106994.744; // udeg/sec
    state->gyronoise = .0001; //rad/loopcylce  // 26010.72428; // stdev of the bias udeg/sec
    state->dtheta = 0;    
    state->dtheta_sigma = state->gyronoise/1000000.0;	    
    state->b = .08; //wheel to wheel is 8 cm.

    state->gopt = getopt_create ();
    getopt_add_bool   (state->gopt, 'h', "help", 0, "Show help");
    getopt_add_bool   (state->gopt, 'g', "use-gyro", 0, "Use gyro for heading instead of wheel encoders");
    getopt_add_string (state->gopt, '\0', "odometry-channel", "BOTLAB_ODOMETRY", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "feedback-channel", "MAEBOT_MOTOR_FEEDBACK", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "sensor-channel", "MAEBOT_SENSOR_DATA", "LCM channel name");
    getopt_add_double (state->gopt, '\0', "alpha", ALPHA_STRING, "Longitudinal covariance scaling factor");
    getopt_add_double (state->gopt, '\0', "beta", BETA_STRING, "Lateral side-slip covariance scaling factor");
    getopt_add_double (state->gopt, '\0', "gyro-rms", GYRO_RMS_STRING, "Gyro RMS deg/s");

    if (!getopt_parse (state->gopt, argc, argv, 1) || getopt_get_bool (state->gopt, "help")) {
        printf ("Usage: %s [--url=CAMERAURL] [other options]\n\n", argv[0]);
        getopt_do_usage (state->gopt);
        exit (EXIT_FAILURE);
    }

    //state->use_gyro = getopt_get_bool (state->gopt, "use-gyro");
    state->use_gyro = 1;
    state->odometry_channel = getopt_get_string (state->gopt, "odometry-channel");
    state->feedback_channel = getopt_get_string (state->gopt, "feedback-channel");
    state->sensor_channel = getopt_get_string (state->gopt, "sensor-channel");
    state->alpha = getopt_get_double (state->gopt, "alpha");
    state->beta = getopt_get_double (state->gopt, "beta");
    state->gyro_rms = getopt_get_double (state->gopt, "gyro-rms") * DTOR;

    // initialize LCM
    state->lcm = lcm_create (NULL);
    maebot_motor_feedback_t_subscribe (state->lcm, state->feedback_channel,
                                       motor_feedback_handler, state);
    maebot_sensor_data_t_subscribe (state->lcm, state->sensor_channel,
                                    sensor_data_handler, state);

    printf ("ticks per meter: %f\n", 1.0/state->meters_per_tick);

    while (1)
        lcm_handle (state->lcm);
}
