README

AUTHORS: Wyatt Felt, Katelyn Fry, Meghan Richey
DUE: 12/14/14

These files represent our submission for the Botlab.

To access, navigate to: src/botlab

To run:

1. Run "make"
2. Navigate to the home rob550_botlabfork folder
3. Run "make"

To execute:
1. Open 2 terminals and copy code onto Maebot
2. In first terminal, connect to Maebot: team1@192.168.3.101
3. Make code on Maebot
4. Type ". setenv.sh"
5. Navigate to /bin folder
6. Type "bot-procman-deputy &"
7. Type "bot-lcm-tunnel"
8. In second terminal, navigate to home rob550_botlabfork
9. Type ". setenv.sh"
10. Navigate to /bin folder
11. Type "bot-procman-deputy &"
12. Type "bot-procman-sheriff ../(_location of config file_) &"
13. Type "bot-lcm-tunnel 192.168.3.10(_X_)"
14. Run the executables from the sheriff

Included files:
1. botlab.c: main functionality
2. camera_lidar.c: lidar projection to video feed
3. odometry.c: pose estimate of robot
4. test_odometry.c: file used to test odometry measurements
5. xyt.h: header file for xyt.c
6. calibration.config: calibration file obtained for camera
7. Makefile: create binaries
8. xyt.c: head2tail and tail2tail conversions
9. xyt_tester.c: standalone to test head2tail and tail2tail conversions
